export const links = [
  { title: "projects", href: "#projects" },
  { title: "email", href: "mailto:moiseenkvs@gmail.com" },
  { title: "resume", href: "#" },
];

export const languages = ["Javascript", "HTML", "Typescript", "CSS", "Python"];
export const libraries = [
  "React",
  "Styled components",
  "Next.js",
  "Jest",
  "React-router",
  "...rest",
];

export const mainProject = {
  title: "Wikipedia Speedrun Game",
  text: "The goal of the game is to navigate from a starting wikipedia article to another one, in the least amount of clicks and time.",
  libraries: ["React", "Redux", "React-router", "Create React App", "Emotion", "Reach UI, Mantine"],
  github: "https://github.com/B0und/WikiSpeedrun",
  live: "https://wikispeedrun.org/",
};

export const projects = [
  {
    title: "Nier loading animation",
    description: "A loading animation from Nier Reincarnation replicated with GSAP.",
    github: "https://github.com/B0und/nierAnimation",
    live: "https://b0und.github.io/nierAnimation/",
  },
  {
    title: "Secret Project #001",
    description: "Currently in development!",
    github: "#",
    live: "#",
  },
  {
    title: "Secret Project #002",
    description: "Currently in development!",
    github: "#",
    live: "#",
  },
];
